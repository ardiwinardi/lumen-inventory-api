<?php

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            $product = new Product();
            $product->id =  Str::uuid();
            $product->name =  $faker->name();
            $product->stock =  10;
            $product->save();
        }
    }
}
