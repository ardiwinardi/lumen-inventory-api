<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductStock;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class ProductStockController extends Controller
{

    public function create(Request $request, $productId)
    {
        $validator = Validator::make($request->all(), [
            'stock_addition' => 'required|numeric',
            'description' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->getMessageBag()], 500);
        }

        $stock_addition = $request->get('stock_addition');

        try {
            DB::beginTransaction();

            $product = Product::findOrFail($productId);
            $product->stock += $stock_addition;
            $product->save();

            $productStock = new ProductStock();
            $productStock->id = Str::uuid();
            $productStock->product_id = $productId;
            $productStock->stock_addition = $stock_addition;
            $productStock->description = $request->get('description');
            $productStock->save();


            DB::commit();
            return response()->json(['message' => 'product stock recorded'], 201);
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return response()->json(['message' => 'product not found'], 404);
        }
    }

    public function get($productId)
    {
        try {
            Product::findOrFail($productId);
            $productStock = ProductStock::where(['product_id' => $productId])->get();
            return response()->json(['data' => $productStock], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'product not found'], 404);
        }
    }
}
