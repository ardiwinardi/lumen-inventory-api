<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class ProductController extends Controller
{

    public function index(Request $request)
    {
        $products = Product::orderBy('created_at', 'desc');
        $count = $products->count();

        $offset = $request->has('offset') ? $request->get('offset') : 0;
        $limit = $request->has('limit') ? $request->get('limit') : 10;

        $products = $products->offset($offset)
            ->limit($limit)
            ->get();

        return response()->json([
            'data' => $products,
            'meta' => [
                'offset' => $offset,
                'limit' => $limit,
                'total' => $count
            ]
        ]);
    }

    public function get($id)
    {

        try {
            $product = Product::findOrFail($id);
            return response()->json(['data' => $product], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'product not found'], 404);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:225',
            'stock' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->getMessageBag()], 500);
        }

        try {
            $product = new Product();
            $product->id =  Str::uuid();
            $product->name =  $request->get('name');
            $product->stock =  $request->get('stock');
            $product->save();

            return response()->json(['message' => 'product created'], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'product not found'], 404);
        }
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:225',
            'stock' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->getMessageBag()], 500);
        }

        try {
            $product = Product::findOrFail($id);
            $product->name = $request->get('name');
            $product->stock = $request->get('stock');
            $product->save();

            return response()->json(['id' => $id, 'message' => 'product updated'], 201);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'product not found'], 404);
        }
    }

    public function destroy($id)
    {
        try {
            Product::findOrFail($id)->delete();
            return response()->json(['id' => $id, 'message' => 'product deleted'], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'product not found'], 404);
        }
    }
}
