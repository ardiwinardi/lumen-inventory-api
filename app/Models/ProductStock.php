<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $keyType = 'string';

    protected $fillable = [
        'id', 'product_id', 'stock_addition', 'description'
    ];
}
